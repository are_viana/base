# Tips for a developer's day-to-day

## Set a commit message guideline
Take advantage of [Git hooks](https://www.atlassian.com/git/tutorials/git-hooks) to set a commit convention for any enviroment.
If you want create better commit messages, regardless if you use ruby, node, python, etc. 
Run this bash script([prepare-commit-msg hook](prepare-commit-msg)) that is following [Conventional commit](https://www.conventionalcommits.org/en/v1.0.0/), but with more custom keys, you can also set you own convention.
> In the root project run:
```
  # Copy the template in the hooks folder
  cp prepare-commit-msg ./.git/hooks/

  # Give permission to run your script
  chmod +x ./.git/hooks/prepare-commit-msg
```

## Create pull request template
[Template example](pull_request_template.md)

Instructions to add your template:
* [Bitbucket](https://bitbucket.org/blog/save-time-with-default-pull-request-descriptions)
* [Github](https://docs.github.com/en/free-pro-team@latest/github/building-a-strong-community/creating-a-pull-request-template-for-your-repository)

## Configure a pipeline

This [bitbucket-pipeline example](bitbucket-pipeline.yml.sample) has the basis steps to:
* Install dependencies
* Run test
* Run one code linter

And also makes a deloy with *capistrano* commands

## API conventions
* Use [JSONApi](https://jsonapi.org/) specification to build apis in json format. 

## Use SchemaSpy 
Use this tool to display your database schema with more details.
1. Unzip the [schemaspy-zip](generate_schemaspy.zip)
2. Follow the Readme.txt instruccions
