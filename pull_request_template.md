## Description
A few sentences describing the overall goals of the pull request's commits.

## Type of change
- [ ] Bug fix
- [x] New feature
- [ ] Code refactor
- [ ] Ready for realese
## Actions
- [ ] The documentation was updated
- [ ] Need to run migrations
- [ ] Has tests 
## Todos
- [ ] Add/Fix Tests
- [ ] Add Documentation

## Steps to Test or Reproduce
Outline the steps to test or reproduce the features related with the PR here.

## Impacted Areas in Application
- [ ] Introduce breaking changes.
### List general components of the application that this PR will affect:
- New one